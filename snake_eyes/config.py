import os
from abc import ABC


class Config(ABC):
    def __init__(self):
        self._cfg = dict()
        self.cfg = self._cfg
        this_path = os.path.dirname(os.path.realpath(__file__))

        self._cfg['common'] = dict()
        self.cfg_common = self._cfg['common']  # Setting shortcut references
        self._cfg['common']['phase'] = 'test'  # 'train', 'test', 'train_test
        self._cfg['common']['temp_dir'] = os.path.join(this_path, '..', 'temp')
        self._cfg['common']['resource_dir'] = os.path.join(this_path, '..', 'resources')

        self._cfg['data'] = dict()
        self.cfg_data = self._cfg['data']  # Setting shortcut references
        self._cfg['data']['in_dir'] = os.path.join(this_path, '..', 'dataset')
        self._cfg['data']['train_portion'] = 0.9
        self._cfg['data']['validation_portion'] = 0.1

        self._cfg['model_test'] = dict()
        self._cfg['model_test']['batch_size'] = 512
        self._cfg['model_test']['epochs'] = 20
        self._cfg['model_test']['model_path'] = os.path.join(self.cfg_common['resource_dir'], 'model_test.h5')

        self._cfg['model_compact'] = dict()
        self._cfg['model_compact']['batch_size'] = 512
        self._cfg['model_compact']['epochs'] = 20
        self._cfg['model_compact']['model_path'] = os.path.join(self.cfg_common['resource_dir'], 'model_compact.h5')

        # Must be the last statement in constructor
        self._check_adjust_config()

    def _check_adjust_config(self):
        """
        Verifies if configuration parameters are correct.
        Also makes necessary correction if possible
        :return:
        """
        assert(self.cfg_data['train_portion'] + self.cfg_data['validation_portion'] == 1.0)
        assert(self.cfg_common['phase'] in ('train', 'test', 'train_test'))
        pass
