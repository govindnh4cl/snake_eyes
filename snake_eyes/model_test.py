from keras.models import Model, load_model
from keras.layers import Input, Conv2D, Dense, Flatten
from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint

# User defined imports
from model_base import ModelBase
from logger import get_logger


log = get_logger()


class ModelTest(ModelBase):
    def __init__(self):
        self.id = 'model_test'
        ModelBase.__init__(self)
        self.m = None

    @staticmethod
    def _get_model():
        inputs = Input(shape=(20, 20, 1))
        x = Conv2D(2, 5, padding='same', activation='relu')(inputs)
        x = Conv2D(8, 5, padding='same', activation='relu')(x)
        x = Conv2D(8, 5, padding='same', activation='relu')(x)
        x = Conv2D(8, 5, padding='same', activation='relu')(x)
        x = Flatten()(x)
        x = Dense(32, activation='relu')(x)
        x = Dense(32, activation='relu')(x)
        x = Dense(16, activation='relu')(x)
        x = Dense(12, activation='softmax')(x)
        outputs = x

        m = Model(inputs=inputs, outputs=outputs)
        print(m.summary())
        return m

    @staticmethod
    def _get_optimizer():
        opt = RMSprop(lr=0.001)
        return opt

    def train(self):
        self.m = self._get_model()

        opt = self._get_optimizer()
        self.m.compile(optimizer=opt, metrics=['accuracy'], loss='categorical_crossentropy')

        cb_chkpt = ModelCheckpoint(self.cfg[self.id]['model_path'], monitor='val_loss', verbose=0, save_best_only=True)

        self.m.fit(x=self.x_train, y=self.y_train,
                   batch_size=self.cfg[self.id]['batch_size'],
                   epochs=self.cfg[self.id]['epochs'],
                   validation_data=(self.x_validation, self.y_validation),
                   callbacks=[cb_chkpt])

    def test(self):
        self.m = load_model(self.cfg[self.id]['model_path'])
        eval_out = self.m.evaluate(self.x_test, self.y_test)
        print('#### Eval out: {}'.format(eval_out))
        pass
