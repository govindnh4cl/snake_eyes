from abc import abstractmethod

# User defined imports
from data import Data
from logger import get_logger

log = get_logger()


class ModelBase(Data):
    def __init__(self):
        Data.__init__(self)
        self.m = None

    @abstractmethod
    def train(self):
        raise NotImplementedError

    @abstractmethod
    def test(self):
        raise NotImplementedError
