import os
import numpy as np
from sklearn.model_selection import train_test_split

# User defined imports
from logger import get_logger
from config import Config

log = get_logger()


class Data(Config):
    def __init__(self):
        Config.__init__(self)

        self.x_train, self.y_train, \
            self.x_validation, self.y_validation,\
            self.x_test, self.y_test = [None] * 6
        self._read_data()  # Read data

        self.mean = None
        self.std = None

    @staticmethod
    def _read_vectors(filename):
        return np.fromfile(filename, dtype=np.uint8).reshape(-1, 401)

    def _read_data(self):
        if 'train' in self.cfg_common['phase']:
            # ------ Setup training and validation sets ----------
            snk = np.vstack(tuple(self._read_vectors(
                os.path.join(self.cfg_data['in_dir'], "snakeeyes_{:02d}.dat".format(nn))) for nn in range(10)))

            x = snk[:, 1:]
            y_ = snk[:, 0]

            # Convert labels to one hot
            y = np.zeros([len(y_), 12], dtype=bool)
            for i in range(len(y_)):
                label = y_[i]
                y[i, label - 1] = True

            self.x_train, self.x_validation, self.y_train, self.y_validation = \
                train_test_split(x, y, test_size=self.cfg_data['validation_portion'], random_state=42, stratify=y)

            self.mean = self.x_train.mean(axis=0)  # Compute data statistics on training data only
            self.std = self.x_train.std(axis=0)

            np.savez_compressed(os.path.join(self.cfg_common['resource_dir'], 'data_stat.npz'),
                                {'mean': self.mean, 'std': self.std})

            # Normalize data to be zero mean and unit variance
            self.x_train = (self.x_train - self.mean)/self.std
            self.x_validation = (self.x_validation - self.mean)/self.std

            # Reshape
            self.x_train = self.x_train.reshape(-1, 20, 20, 1)
            self.x_validation = self.x_validation.reshape(-1, 20, 20, 1)

        if 'test' in self.cfg_common['phase']:
            # ------ Setup test set ----------
            snk_test = self._read_vectors(os.path.join(self.cfg_data['in_dir'], "snakeeyes_test.dat"))
            x_test = snk_test[:, 1:]
            y_test = snk_test[:, 0]

            # Convert labels to one hot
            self.y_test = np.zeros([len(y_test), 12], dtype=bool)
            for i in range(len(y_test)):
                label = y_test[i]
                self.y_test[i, label - 1] = True

            data_stat = np.load(os.path.join(self.cfg_common['resource_dir'], 'data_stat.npz'))
            self.mean = data_stat['arr_0'].item()['mean']
            self.std = data_stat['arr_0'].item()['std']

            self.x_test = (x_test - self.mean)/self.std  # Normalize
            self.x_test = self.x_test.reshape(-1, 20, 20, 1)  # Reshape


