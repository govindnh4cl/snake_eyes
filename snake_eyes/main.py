import os
import configparser

import numpy as np
import tensorflow as tf

my_random_seed = 1331
np.random.seed(my_random_seed)
tf.set_random_seed(my_random_seed)

# User defined imports
from logger import setup_logger
from model_test import ModelTest
from model_compact import ModelCompact

log = setup_logger(None, 0)  # Initialize logger

if __name__ == '__main__':
    m = ModelTest()

    if 'train' in m.cfg_common['phase']:
        m.train()

    if 'test' in m.cfg_common['phase']:
        m.test()
