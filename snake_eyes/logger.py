import os
import logging

name = ''


def setup_logger(instance_id, log_option=0):
    """

    :param instance_id: Unique identifier in case of file-write
    :param log_option: 0 for console, 1 for file-write too
    :return:
    """
    log = logging.getLogger(name)
    formatter = logging.Formatter('{asctime:15s} {levelname:8s} {message}', style='{')
    log.setLevel(logging.DEBUG)

    if log_option > 0:
        log_dir = 'logs'
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)

        log_file = os.path.join(log_dir, instance_id + ".log")
        fh = logging.FileHandler(log_file)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        log.addHandler(fh)
        log.info('Dumping logs to file: {:s}'.format(log_file))

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    log.addHandler(ch)

    return log


def get_logger():
    return logging.getLogger(name)


