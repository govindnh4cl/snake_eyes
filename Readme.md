See the kaggle dataset details here: https://www.kaggle.com/nicw102168/snake-eyes

**Setup**:

1. Clone/download repository

2. Keep data files (`snakeeyes_00.dat, ..., snakeeyes_09.dat, snakeeyes_test.dat`) inside the `dataset` directory

3. For running inference using pre-trained model, execute `python snake_eyes/main.py`.

4. Wanna do something else: configuration parameters are kept in `nake_eyes/config.py`. Update and run.

**My setup**:

* Keras with tensorflow backend

* Numpy, sklearn

* Nvidia GTX 960 (2 GB)

**Dataset**:

* Training: 900,000 samples

* Validation: 100,000 samples

* Test: 10,000 samples 

* Note: **The test data was never used for fine-tuning the hyper-parameters of network. The best performing network on validation set was directly applied on test set.**

* Remarks:

    * I feel that not all 900,000 samples are necessary for training the model. Maybe using just 100,000 samples could also be enough.

**Preprocessing**:

* Out of 400 features, Each feature was normalized to zero mean and unit varience across entire training dataset. Validation set was not used for finding mean and variance.

**Network**:

* Number of trainable parameters: 107,896

* 4 conv layers

* 4 fully-connected layers

* Loss: categorical-crossentropy

* optimizer: RMSProp

* Epochs: 20

* Inference Model: During inference stage, the model with least validation loss is used to make predictions

* Model size (space required to store trained model): < 1MB 

* Remarks: 

    * I do feel that this model has too many parameters and number of parameters can be reduced further.

    * The large dataset size itself works as a regularizer and overfitting doesn't occur.

**Log**:

    $ python snake_eyes/main.py 
    Using TensorFlow backend.
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_1 (InputLayer)         (None, 20, 20, 1)         0         
    _________________________________________________________________
    conv2d_1 (Conv2D)            (None, 20, 20, 2)         52        
    _________________________________________________________________
    conv2d_2 (Conv2D)            (None, 20, 20, 8)         408       
    _________________________________________________________________
    conv2d_3 (Conv2D)            (None, 20, 20, 8)         1608      
    _________________________________________________________________
    conv2d_4 (Conv2D)            (None, 20, 20, 8)         1608      
    _________________________________________________________________
    flatten_1 (Flatten)          (None, 3200)              0         
    _________________________________________________________________
    dense_1 (Dense)              (None, 32)                102432    
    _________________________________________________________________
    dense_2 (Dense)              (None, 32)                1056      
    _________________________________________________________________
    dense_3 (Dense)              (None, 16)                528       
    _________________________________________________________________
    dense_4 (Dense)              (None, 12)                204       
    =================================================================
    Total params: 107,896
    Trainable params: 107,896
    Non-trainable params: 0
    _________________________________________________________________
    None
    Train on 900000 samples, validate on 100000 samples
    Epoch 1/20
    2018-03-31 23:26:21.694852: I tensorflow/core/platform/cpu_feature_guard.cc:137] Your CPU supports instructions that this TensorFlow binary was not compiled to use: SSE4.1 SSE4.2 AVX AVX2 FMA
    2018-03-31 23:26:23.364737: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:892] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
    2018-03-31 23:26:23.365300: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1030] Found device 0 with properties: 
    name: GeForce GTX 960 major: 5 minor: 2 memoryClockRate(GHz): 1.367
    pciBusID: 0000:01:00.0
    totalMemory: 1.95GiB freeMemory: 1.52GiB
    2018-03-31 23:26:23.365346: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1120] Creating TensorFlow device (/device:GPU:0) -> (device: 0, name: GeForce GTX 960, pci bus id: 0000:01:00.0, compute capability: 5.2)
    900000/900000 [==============================] - 66s 73us/step - loss: 1.2285 - acc: 0.4998 - val_loss: 0.6440 - val_acc: 0.7247
    Epoch 2/20
    900000/900000 [==============================] - 54s 60us/step - loss: 0.5899 - acc: 0.7500 - val_loss: 0.3495 - val_acc: 0.8565
    Epoch 3/20
    900000/900000 [==============================] - 54s 60us/step - loss: 0.3249 - acc: 0.8739 - val_loss: 0.1472 - val_acc: 0.9501
    Epoch 4/20
    900000/900000 [==============================] - 54s 61us/step - loss: 0.1719 - acc: 0.9405 - val_loss: 0.0898 - val_acc: 0.9691
    Epoch 5/20
    900000/900000 [==============================] - 54s 60us/step - loss: 0.0962 - acc: 0.9691 - val_loss: 0.0573 - val_acc: 0.9806
    Epoch 6/20
    900000/900000 [==============================] - 53s 59us/step - loss: 0.0590 - acc: 0.9816 - val_loss: 0.0249 - val_acc: 0.9925
    Epoch 7/20
    900000/900000 [==============================] - 52s 58us/step - loss: 0.0394 - acc: 0.9880 - val_loss: 0.0280 - val_acc: 0.9908
    Epoch 8/20
    900000/900000 [==============================] - 51s 57us/step - loss: 0.0269 - acc: 0.9919 - val_loss: 0.0094 - val_acc: 0.9971
    Epoch 9/20
    900000/900000 [==============================] - 51s 56us/step - loss: 0.0190 - acc: 0.9944 - val_loss: 0.0081 - val_acc: 0.9977
    Epoch 10/20
    900000/900000 [==============================] - 50s 55us/step - loss: 0.0142 - acc: 0.9959 - val_loss: 0.0081 - val_acc: 0.9977
    Epoch 11/20
    900000/900000 [==============================] - 49s 54us/step - loss: 0.0106 - acc: 0.9969 - val_loss: 0.0132 - val_acc: 0.9961
    Epoch 12/20
    900000/900000 [==============================] - 47s 53us/step - loss: 0.0083 - acc: 0.9976 - val_loss: 0.0042 - val_acc: 0.9989
    Epoch 13/20
    900000/900000 [==============================] - 46s 52us/step - loss: 0.0071 - acc: 0.9980 - val_loss: 0.0068 - val_acc: 0.9982
    Epoch 14/20
    900000/900000 [==============================] - 45s 50us/step - loss: 0.0067 - acc: 0.9982 - val_loss: 0.0044 - val_acc: 0.9989
    Epoch 15/20
    900000/900000 [==============================] - 45s 50us/step - loss: 0.0053 - acc: 0.9985 - val_loss: 0.0044 - val_acc: 0.9989
    Epoch 16/20
    900000/900000 [==============================] - 45s 49us/step - loss: 0.0049 - acc: 0.9987 - val_loss: 0.0446 - val_acc: 0.9889
    Epoch 17/20
    900000/900000 [==============================] - 44s 49us/step - loss: 0.0043 - acc: 0.9988 - val_loss: 0.0044 - val_acc: 0.9989
    Epoch 18/20
    900000/900000 [==============================] - 44s 48us/step - loss: 0.0044 - acc: 0.9989 - val_loss: 0.0041 - val_acc: 0.9989
    Epoch 19/20
    900000/900000 [==============================] - 43s 48us/step - loss: 0.0039 - acc: 0.9990 - val_loss: 0.0082 - val_acc: 0.9983
    Epoch 20/20
    900000/900000 [==============================] - 43s 48us/step - loss: 0.0038 - acc: 0.9990 - val_loss: 0.0029 - val_acc: 0.9993
    10000/10000 [==============================] - 1s 54us/step
    #### Eval out: [0.0033687273369969717, 0.9993]

**Results**:

* Because of the high number of samples, the first epoch itself is able to drive the validation accuracy to > 70%

* The test-accuracy is 99.93%. See the log with `#### Eval out: ` string.

